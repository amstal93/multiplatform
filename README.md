Multiplatform Image
===============

## Overview ##

_Docker_ images that contain tools and components required for **building** of _multi-platform_ projects.

## Tags ##

Below are listed tags for which are **available** their corresponding **images**:

- **[sdk](https://gitlab.com/docker-universum/multiplatform/wikis/SDK)**
- **[api-xx](https://gitlab.com/docker-universum/multiplatform/wikis/API)**

> For example image which provides configuration with **Android API 29** may be used as: `image: universumstudios/multiplatform:api-29`.

## Cloud ##

Images are available via **[Docker Cloud](https://cloud.docker.com/swarm/universumstudios/repository/docker/universumstudios/multiplatform/general)**.

## [License](https://gitlab.com/docker-universum/multiplatform/blob/master/LICENSE.md) ##

**Copyright 2020 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.